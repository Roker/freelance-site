<?php
use Migrations\AbstractMigration;

class RenameHours extends AbstractMigration
{
    public function up()
    {

		$this->table('hours')
			->rename('shifts')
			->save();

    }
}