<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Date;


/**
 * Shifts Controller
 *
 * @property \App\Model\Table\ShiftsTable $Shifts
 *
 * @method \App\Model\Entity\Shift[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShiftsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $shifts = $this->paginate($this->Shifts);

        $this->set(compact('shifts'));
    }

    /**
     * View method
     *
     * @param string|null $id Shift id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $shift = $this->Shifts->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('shift', $shift);
    }*/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($user_id)
    {
        $shift = $this->Shifts->newEntity();
		
		$this->LoadModel('UserWageItems');
		$this->LoadModel('PayPeriods');

		$this->set('user_wage_items', $this->UserWageItems->find('list')->where(['user_id' => $user_id])->andWhere(['enabled' => true])->toArray());
		
		
        if ($this->request->is('post')) {
			
            $shift = $this->Shifts->patchEntity($shift, $this->request->getData());
					
			if(!isset($shift['user_wage_item_id'])){
				$this->Flash->error(__('Error finding wage item. Please contact your administrator.'));
			}else{
				if($shift['date'] != null){
					$shift['user_id'] = $user_id;
					$shift['pay_period_id'] = $this->PayPeriods->GetPayPeriodId($shift['date']);

					$this->CheckBillingDate($shift['date']);
					
					if ($this->Shifts->save($shift)) {
						$this->Flash->success(__('The shift has been saved.'));
						if(isset($this->request->getData()['newShift'])){
							return $this->redirect(['controller' => 'Shifts', 'action' => 'add', $user_id]);
						}
						return $this->redirect(['controller' => 'Users', 'action' => 'home']);
					}
				}
				
				$this->Flash->error(__('The shift could not be saved. Please, try again.'));
			}
        }
        $this->set(compact('shift', 'user_id', 'start_date', 'end_date', 'options'));
    }

	private function CheckBillingDate($shift_date){
		$this->GetBillingPeriod($start_date, $end_date);
		
		if($shift_date < $start_date){
			$this->Flash->error(__('Shift date prior to billing period start! Shift will need to be paid manually by your administrator.'));
			//$headers =  'MIME-Version: 1.0' . "\r\n"; 
			//$headers .= 'From: Your name <info@focus-av.com>' . "\r\n";
			//$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			//mail("andrewbailey@shaw.ca","Late p","Late p", $headers);
		}
	}
    /**
     * Edit method
     *
     * @param string|null $id Shift id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shift = $this->Shifts->get($id, [
            'contain' => []
        ]);
		
		$this->LoadModel('PayPeriods');
		$this->LoadModel('UserWageItems');
		$this->set('user_wage_items', $this->UserWageItems->find('list')->where(['user_id' => $shift['user_id']])->andWhere(['enabled' => true])->toArray());

        if ($this->request->is(['patch', 'post', 'put'])) {
			if(!isset($shift['user_wage_item_id'])){
				$this->Flash->error(__('Error finding wage item. Please contact your administrator.'));
			}else{
				$shift = $this->Shifts->patchEntity($shift, $this->request->getData());
				$shift['pay_period_id'] = $this->PayPeriods->GetPayPeriodId($shift['date']);
			
				//return debug($shift);
				$this->CheckBillingDate($shift['date']);

				if ($this->Shifts->save($shift)) {
					$this->Flash->success(__('The shift has been saved.'));

					return $this->redirect(['controller' => 'Users', 'action' => 'home']);
				}
				$this->Flash->error(__('The shift could not be saved. Please, try again.'));
			}
            
        }
        $users = $this->Shifts->Users->find('list', ['limit' => 200]);
        $this->set(compact('shift', 'users'));
    }
	
	// public function fixPayPeriodIds(){
		// $this->LoadModel('PayPeriods');
		
		// $shifts = $this->Shifts->find('all');
		
		// foreach($shifts as $shift){
			// $shift['pay_period_id'] = $this->PayPeriods->GetPayPeriodId($shift['date']);
			// $this->Shifts->save($shift);
		// }
	// }

    /**
     * Delete method
     *
     * @param string|null $id Shift id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shift = $this->Shifts->get($id);
        if ($this->Shifts->delete($shift)) {
            $this->Flash->success(__('The shift has been deleted.'));
        } else {
            $this->Flash->error(__('The shift could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    }
	
	public function isAuthorized($user)
	{
		if($user['can_edit']){
			return true;
		}else{
			if ($this->request->getParam('action') === 'edit' || $this->request->getParam('action') === 'delete') {
				$id = $this->request->getParam('pass');
				if($id){
					 $shift = $this->Shifts->get($id);
					 if($shift['user_id'] == $user['id'] && !$shift['approved']){
						 return true;
					 }
				}
			}else if ($this->request->getParam('action') === 'add') {
				$id = $this->request->getParam('pass')[0];
				if($id){
					 if($user['id'] == $id){
						 return true;
					 }
				}
			}
		}	
		return false;
	}

}
