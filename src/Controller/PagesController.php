<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

	public function help(){
		$helpSettingName = 'helptext';
		$this->loadModel('Settings');
		
		$helptext = $this->Settings->find('all')->where(['name' => $helpSettingName])->first();
		
		if($helptext == null){
			$helptext = $this->Settings->newEntity();
			$helptext->name = $helpSettingName;
		}
		$this->set('text' , $helptext->value);
	}
	
	public function editHelp(){
		$helpSettingName = 'helptext';
		$this->loadModel('Settings');

		$helptext = $this->Settings->find('all')->where(['name' => $helpSettingName])->first();
		
		if($helptext == null){
			$helptext = $this->Settings->newEntity();
			$helptext->name = $helpSettingName;
		}
		
		if($this->request->is('post')) {
			$helptext->value = $this->request->getData()['text'];
			
			$this->Settings->save($helptext);
			$this->redirect('/pages/help');
		}
		
		$this->set('text' , $helptext->value);		
	}
	
	public function isAuthorized($user)
	{
		if($user['can_edit']){
			return true;
		}else if ($this->request->getParam('action') === 'help'){
			return true;
		}
		return false;
	}
}
