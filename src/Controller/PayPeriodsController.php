<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PayPeriods Controller
 *
 * @property \App\Model\Table\PayPeriodsTable $PayPeriods
 *
 * @method \App\Model\Entity\PayPeriod[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PayPeriodsController extends AppController
{

    /**
     * View User method
     *
     * @return \Cake\Http\Response|void
     */
    public function viewUser($user_id)
    {
		$this->LoadModel('Users');

		$user = $this->Users->get($user_id);
		
		$payPeriods = $this->PayPeriods->find('all')->contain('Shifts', function ($q) use ($user) {
								return $q
									->where(['Shifts.user_id' => $user['id']]);
							})->distinct(['PayPeriods.id'])->order(['PayPeriods.start_date' => 'DESC']);
				
		$payPeriods->matching('shifts', function ($q) use ($user) {
			return $q->where(['Shifts.user_id' => $user['id']]);
		});
		
        $payPeriods = $this->paginate($payPeriods);

        $this->set(compact('payPeriods'));
		$this->set('user', $user);
	}
	
	/**
     * View User Shifts method
     *
     * @return \Cake\Http\Response|void
     */
    public function viewUserPayPeriod($user_id, $pay_period_id)
    {
		$this->LoadModel('Users');

		$user = $this->Users->get($user_id);

		
		//return debug($pay_period);
		$this->set('pay_period_id', $pay_period_id);
		$this->set('user', $user);
    }
	
	public function printUserPayPeriod($user_id, $pay_period_id)
    {
		$this->LoadModel('Users');

		$user = $this->Users->get($user_id);


		$this->set('pay_period_id', $pay_period_id);
		$this->set('user', $user);
		$this->viewBuilder()->setLayout(false);
    }
	
	public function adminView($pay_period_id){
		$this->LoadModel('Users');
		
		$users = $this->Users->find('all')->matching('Shifts', function ($q) use ($pay_period_id){
																			return $q->matching('PayPeriods', function ($q) use ($pay_period_id){ return $q->where(['PayPeriods.id' => $pay_period_id]);});
																		})->distinct('Users.id')->contain('Shifts');
		
		$pay_period = $this->PayPeriods->get($pay_period_id);
		$this->set('start_date', $pay_period->start_date);
		$this->set('end_date', $pay_period->end_date);
		$this->set('pay_period_id', $pay_period_id);
		$this->set('users', $users);
	}
	
	public function adminViewPrintable($pay_period_id){
		$this->LoadModel('Users');
		
		$users = $this->Users->find('all')->matching('Shifts', function ($q) use ($pay_period_id){
																			return $q->matching('PayPeriods', function ($q) use ($pay_period_id){ return $q->where(['PayPeriods.id' => $pay_period_id]);});
																		})->distinct('Users.id')->contain('Shifts');
		
		$pay_period = $this->PayPeriods->get($pay_period_id);
		$this->set('start_date', $pay_period->start_date);
		$this->set('end_date', $pay_period->end_date);
		$this->set('pay_period_id', $pay_period_id);
		$this->set('users', $users);
				$this->viewBuilder()->setLayout(false);

	}
	
	public function adminViewAll(){
		$payPeriods = $this->PayPeriods->find('all')->contain('Shifts')->order(['start_date' => 'DESC']);
		$this->set('payPeriods', $payPeriods);
	}
	
	public function isAuthorized($user)
	{
		$action = $this->request->getParam('action');
		if($this->request->getParam('pass') && count($this->request->getParam('pass')) > 0){
			$id = $this->request->getParam('pass')[0];
		}
		
		switch($action){
			case 'viewUser':
			case 'viewUserPayPeriod':
			case 'printUserPayPeriod':
				if($user['id'] == $id || $user['can_approve'] || $user['can_edit']){
					return true;
				}
				break;
			case 'adminView':
			case 'adminViewPrintable':
			case 'adminViewAll':
				if($user['can_approve'] || $user['can_edit']){
					return true;
				}
				break;
		}
		return parent::isAuthorized($user);
	}
}
