<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
	public function initialize()
    {
        parent::initialize();
		
        $this->Auth->allow('check');
    }
	
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		
		$users = $this->Users->find('all', ['order' => 'name']);
		$hideInactive = (!$this->request->is(['get']) || !isset($_GET['showInactive']) || $_GET['showInactive'] != true);
		if($hideInactive) {
			$users = $users->where(['active' => true]);
		}
		
        //$users = $this->paginate($users->order(['name']));

        $this->set(compact('users', 'hideInactive'));
		$this->render('index_no_pages');

		
    }
	
	public function check(){
		$users = $this->Users->find('all');
		
		foreach($users as $user){
			if($user['password'] == 'password'){
				$user['password'] = 'focusav';
				$this->Users->save($user);
			}
		}
		return null;
	}

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$this->GetBillingPeriod($start_date, $end_date);
				
        $user = $this->Users->get($id);
				
        $this->set(compact('user', 'start_date', 'end_date'));
    }
	
	public function shifts($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Shifts' => ['sort' => ['Shifts.date' => 'DESC']]]
        ]);
		

        $this->set('user', $user);
    }
	
	public function payPeriods($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Shifts' => ['group' => ['Shifts.date' => 'DESC']]]
        ]);
		

        $this->set('user', $user);
    }
	
	public function home()
    {	
		if(!$this->Auth->user()){
			return $this->redirect(['action' => 'login']);
		}else{
			return $this->redirect(['action' => 'view', $this->Auth->user('id')]);
		}
    }
	
		
	public function setPassword(){
		$user = $this->Users->get($this->Auth->user('id'));
		$this->set('user', $user);
		
		if ($this->request->is(['post', 'put'])) {
			if (!(new DefaultPasswordHasher)->check($this->request->getData('current_password'), $user->password)) {
				$this->Flash->error(__('Incorrect password.'));
				return;
			}

            if($this->request->getData('password') != $this->request->getData('password_confirm')){
				$this->Flash->error(__('Passwords do not match.'));
				return;
			}
			$this->Users->patchEntity($user, $this->request->getData());
			
			if ($this->Users->save($user)) {
                $this->Flash->success(__('Password successfully changed.'));

                return $this->redirect(['action' => 'home']);
            }
            $this->Flash->error(__('Password could not be changed.'));
        }
	}
	
	public function resetPassword($id){
		$user = $this->Users->get($id);
		
		$password = $this->generateRandomString(5);
		$user->password = $password;
		
		if ($this->Users->save($user)) {
			$this->Flash->success(__('Password successfully reset.'));
			$this->set('user', $user);
			$this->set('password', $password);
		}else{
			$this->Flash->error(__('Password could not be reset.'));
			 return $this->redirect(['action' => 'index']);
		}
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function approve(){
		$this->GetBillingPeriod($start_date, $end_date, -5);

		$query = $this->Users->find('all')->contain(['Shifts' => 
									function ($query) use($end_date) {
											return $query->where(['approved' => false])->andWhere(['date <=' => $end_date])->order(['date' => 'ASC']);
										}
										])->matching('Shifts', function ($q) use ($end_date)  {
			return $q->where(['approved' => false])->where(['date <=' => $end_date]);
		})->distinct('Users.id');
		
		$users = $query->toArray();
		
		
		if ($this->request->is(['post', 'put'])) {
            $patched = $this->Users->patchEntities($users, $this->request->getData(), ['associated' => 'shifts']);
			foreach ($patched as $user) {
				$user->setDirty('shifts', true);
				$result = $this->Users->save($user, ['associated' => ['Shifts']]);
				if($result == false){
					$this->Flash->error(__('A Shift did not save correctly. Employee: ' . $user->name));
				}
			}
			$this->Flash->success(__('Shifts updated.'));
        }
		
		$this->set('users', $users);
		$this->set('start_date', $start_date);
		$this->set('end_date', $end_date);
	}

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), [
						'associated' => ['UserWageItems']
					]);
            if ($this->Users->save($user)) {

                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserWageItems']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
			for($i=0;$i<count($user->user_wage_items);$i++){

				if($user->user_wage_items[$i]->id != $this->request->getData()['user_wage_items'][$i]['id']){
					$this->Flash->error(__('The user could not be saved. Please, try again.'));
					return $this->redirect($this->referer());
				}
			}
            $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['UserWageItems']]);
			
			if($user->isDirty('can_approve') && !$this->Auth->user('can_approve')){
				$user->dirty('can_approve', false);
			}

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }


    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	/**
     * Add-wage-item method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addWageItem($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserWageItems']
        ]);

		$wageItemsTable = TableRegistry::getTableLocator()->get('UserWageItems');
		$newWageItem = $wageItemsTable->newEntity();
		$newWageItem->user_id = $user->id;
		$newWageItem->enabled = true;
        if ($this->request->is(['patch', 'post', 'put'])) {
			$wageItemsTable->patchEntity($newWageItem, $this->request->getData());
			if ($wageItemsTable->save($newWageItem)) {
                $this->Flash->success(__('Wage item added successfully.'));

                return $this->redirect(['action' => 'edit', $user->id]);
            }else{
				$this->Flash->error(__('The wage item could not be added. Please, try again.'));
			}
			echo debug($newWageItem);
        }
        $this->set(compact('user'));
        $this->set('newWageItem', $newWageItem);
    }
	/**
     * edit-wage-item method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function editWageItem($id = null)
    {
		// $this->request->allowMethod(['post', 'delete']);
		$wageItemsTable = TableRegistry::getTableLocator()->get('UserWageItems');
        $wageItem = $wageItemsTable->get($id);
		$userId = $wageItem->user_id;

        if ($this->request->is(['patch', 'post', 'put'])) {
			$wageItemsTable->patchEntity($wageItem, $this->request->getData());
			if ($wageItemsTable->save($wageItem)) {
                $this->Flash->success(__('Wage item saved successfully.'));

                return $this->redirect(['action' => 'edit', $userId]);
            }else{
				$this->Flash->error(__('The wage item could not be saved. Please, try again.'));
			}
			return $this->redirect(['action' => 'edit', $userId]);
        }
        $this->set('wageItem', $wageItem);

    }
	/**
     * delete-wage-item method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function deleteWageItem($id = null)
    {
		// $this->request->allowMethod(['post', 'delete']);
		$wageItemsTable = TableRegistry::getTableLocator()->get('UserWageItems');
        $userWageItem = $wageItemsTable->get($id);
		$userId = $userWageItem->user_id;
        if ($wageItemsTable->delete($userWageItem)) {
            $this->Flash->success(__('The wage item has been deleted.'));
        } else {
            $this->Flash->error(__('The wage item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'edit', $userId]);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
				if(!$user['active']){
					$this->Flash->error(__('This account is inactive. Please contact your administrator.'));
					return $this->redirect(['action' => 'login']);
				}
                $this->Auth->setUser($user);
				
				if($this->request->data('remember_me')){
					$this->Cookie->configKey('CookieAuth', [
						'expires' => '+1 year',
						'httpOnly' => true
					]);
					$this->Cookie->write('CookieAuth', [
						'email' => $this->request->data('email'),
						'password' => $this->request->data('password')
					]);
				}

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid email or password, try again'));
        }else{
			if ($this->Auth->user()) {
				return $this->redirect(['action' => 'home']);
			}	
		}
    }
	


    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
	
	public function isAuthorized($user)
	{
		$action = $this->request->getParam('action');
		if($this->request->getParam('pass') && count($this->request->getParam('pass')) > 0){
			$id = $this->request->getParam('pass')[0];
		}
		
		switch($action){
			case 'home':
			case 'shifts':
			case 'login':
			case 'logout':
			case 'setPassword':
				return true;
			case 'view':
				if($user['id'] == $id || $user['can_approve']){
					return true;
				}
				break;
			case 'edit':
			case 'add':
				if ($user['can_edit']) {
					return true;
				}
				break;
			case 'addWageItem':
			case 'editWageItem':
			case 'deleteWageItem':
			case 'approve':
			case 'index':
				if ($user['can_approve']) {
					return true;
				}
				break;
			case 'resetPassword':
			case 'delete':
				if ($user['can_edit']) {
					return true;
				}
				break;
		}
		return parent::isAuthorized($user);
	}
}
