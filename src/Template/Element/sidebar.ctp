<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php 
			echo '<li>';
			echo $this->Html->link(__('Home'), ['controller' => 'Users', 'action' => 'home']);
			echo '</li>';
			echo '<li>';
			echo $this->Html->link(__('View Past Shifts'), ['controller' => 'PayPeriods', 'action' => 'view-user', $Auth->user('id')]);
			echo '</li>';
			echo '<li>';
			echo $this->Html->link(__('New Shift'), ['controller' => 'Shifts', 'action' => 'add', $Auth->user('id')]);
			echo '</li>';
			echo '<li>';
			echo $this->Html->link(__('Set Password'), ['controller' => 'users', 'action' => 'setPassword']);
			echo '</li>';
			echo '<li>';
			echo $this->Html->link(__('Help'), ['controller' => 'pages', 'action' => 'help']);
			echo '</li>';
			echo '<li>';
			echo $this->Html->link(__('Documents'), ['controller' => 'uploads', 'action' => 'index']);
			echo '</li>';


			if($Auth->user('can_approve')){
				echo '<hr>';
				echo  '<li class="heading">' .  __('Admin') . '</li>';
				echo '<li>';
				echo $this->Html->link(__('Approve Shifts'), ['controller' => 'users', 'action' => 'approve']);
				echo '</li>';
				echo '<li>';
				echo $this->Html->link(__('View Employees'), ['controller' => 'users', 'action' => 'index']);
				echo '</li>';
				echo '<li>';
				echo $this->Html->link(__('Add Employee'), ['controller' => 'users', 'action' => 'add']);
				echo '</li>';
				echo '<li>';
				echo $this->Html->link(__('View Pay Periods'), ['controller' => 'pay_periods', 'action' => 'admin-view-all']);
				echo '</li>';
				if($Auth->user('can_edit')){
					echo '<li>';
					echo $this->Html->link(__('Upload Document'), ['controller' => 'uploads', 'action' => 'add']);
					echo '</li>';
				}
			}
		?>
    </ul>
</nav>
