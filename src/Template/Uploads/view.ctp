<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upload $upload
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Upload'), ['action' => 'edit', $upload->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Upload'), ['action' => 'delete', $upload->id], ['confirm' => __('Are you sure you want to delete # {0}?', $upload->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Uploads'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Upload'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="uploads view large-9 medium-8 columns content">
    <h3><?= h($upload->name) ?></h3>
	<?= $this->Html->link(__('Download'), ['action' => 'download', $upload->id]) ?>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($upload->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($upload->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Path') ?></th>
            <td><?= h($upload->path) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File') ?></th>
            <td><?= h($upload->file) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($upload->id) ?></td>
        </tr>
		
    </table>
</div>
