<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3>Help</h3>
	<?= $this->Form->create() ?>		
    <fieldset>
        <?php
            echo $this->Form->textarea('text', ['value' => $text]);
        ?>
    </fieldset>
    <?= $this->Form->button('Submit'); ?>
    <?= $this->Form->end() ?>
</div>
