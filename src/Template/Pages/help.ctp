<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3>Help</h3>
	<p><?= nl2br($text) ?> </p>
	<?php
	if($Auth->user('can_edit')) :
	?>
    <?= $this->Html->link('Edit', ['controller' => 'pages', 'action' => 'editHelp'] ); ?>
	<?php endif; ?>
</div>
	