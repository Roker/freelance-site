<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h('Pay Periods') ?></h3>
	<table>
		<thead>
			<tr>
				<th scope="col" style="width: 200px"><?= $this->Paginator->sort('date') ?></th>
				<th scope="col" style="width: 100px"><?=__('Employees')?> </th>
				<th scope="col" style="width: 100px"><?=__('Shifts')?> </th>
				<th scope="col" style="width: 100px"><?=__('Hours')?> </th>
				<th scope="col" style="width: 100px"><?=__('Earned')?> </th>
			</tr>
		</thead>
		<tbody>
			<?php 
			
			foreach ($payPeriods as $pay_period){
				$users = array();
				$pay_period['hours'] = 0;
				$pay_period['earned'] = 0;
				foreach($pay_period['shifts'] as $shift){
					$pay_period['hours'] += $shift['hours'];
					$pay_period['earned'] += ($shift['hours'] * $shift['rate']);
					if(!in_array($shift['user_id'], $users)){
						array_push($users, $shift['user_id']);
					}
				}
				echo '<tr>';
				echo '<td>' . $this->Html->link($pay_period->start_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) 
						. ' - ' . $pay_period->end_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]),
						['controller' => 'pay_periods', 'action' => 'admin_view', $pay_period->id]) . '</td>';
				echo '<td>' . count($users) . '</td>';
				echo '<td>' . count($pay_period['shifts']) . '</td>';
				echo '<td>' . $pay_period['hours'] . '</td>';
				echo '<td>$' . $pay_period['earned'] . '</td>';
				echo '</tr>';
			} ?>
			
		</tbody>
	</table>
</div>