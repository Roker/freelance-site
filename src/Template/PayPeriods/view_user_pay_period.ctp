<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">

    <h3><?= h($user->name) ?></h3>
	<h5><?= $this->Html->link('View Printable Summary', ['controller' => 'PayPeriods', 'action' => 'printUserPayPeriod', $user->id, $pay_period_id]);?></h5>

	<?=$this->cell('Shifts::displayPayPeriodWithSummary', [$user->id, $pay_period_id]);?>
	
</div>
