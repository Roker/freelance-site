<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<style>
h3 {
	margin:0px;
}
table, th, td {
	border:1px solid black;
}
table{
	width:100%;
}
.summary{
	text-align:right;
	margin:0px;
}
</style>
<?=$this->cell('Shifts::displayPayPeriodWithSummary', [$user->id, $pay_period_id, true]);?>
