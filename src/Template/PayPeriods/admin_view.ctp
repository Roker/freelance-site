<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
	<?= $this->Html->link(__('Back'), ['controller' => 'pay_periods', 'action' => 'admin-view-all']); ?>
	<br/>
	<?= $this->Html->link(__('View Printable Summary'), ['controller' => 'pay_periods', 'action' => 'admin-view-printable', $pay_period_id]); ?>
	<h3><?= __('Pay Period: ') . $start_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) . ' - ' . $end_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]);?> </h3>
	<?php
	foreach($users as $user){
		echo '<h4>' . __($user->name) . '</h4>';
		echo $this->cell('Shifts::displayPayPeriod', [$user->id, $pay_period_id]);
	}
	?>
	
</div>
