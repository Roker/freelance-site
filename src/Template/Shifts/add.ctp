<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shift $shift
 */
?>
<?=$this->element('sidebar');?>
<div class="shifts form large-9 medium-8 columns content">
    <?= $this->Form->create($shift) ?>
    <fieldset>
        <legend><?= __('Add Shift') ?></legend>
        <?php
            echo $this->Form->control('date');
            echo $this->Form->control('start_time', ['value' => 0, 'interval' => 5]);
            echo $this->Form->control('end_time', ['value' => 0, 'interval' => 5]);
			if(isset($user_wage_items)){
				if(count($user_wage_items) > 1){
					echo $this->Form->control('user_wage_item_id', ['options' => $user_wage_items, 'label' => 'Wage Item']);
				}else{
					echo $this->Form->control('user_wage_item_id', ['options' => $user_wage_items, 'hidden' => true, 'label' => '']);
				}
			}
            echo $this->Form->control('notes', ['required' => false]);
        ?>
    </fieldset>
    <?= $this->Form->button('Submit', ['type' => 'submit']); ?>
	<?= $this->Form->button('Submit and New Shift', ['type' => 'submit', 'name' => 'newShift', 'style' => 'margin-right: 50px']); ?>
    <?= $this->Form->end() ?>
</div>
