<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shift $shift
 */
?>
<?=$this->element('sidebar');?>
<div class="shifts form large-9 medium-8 columns content">
    <?= $this->Form->create($shift) ?>
    <fieldset>
        <legend><?= __('Edit Shift') ?></legend>
        <?php
            echo $this->Form->control('date');
            echo $this->Form->control('start_time', ['interval' => 5]);
            echo $this->Form->control('end_time', ['interval' => 5]);
			if(isset($user_wage_items)){
				if(count($user_wage_items) > 1){
					echo $this->Form->control('user_wage_item_id', ['options' => $user_wage_items, 'label' => 'Wage Item']);
				}else{
					echo $this->Form->control('user_wage_item_id', ['options' => $user_wage_items, 'hidden' => true, 'label' => '']);
				}
			}            echo $this->Form->control('notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
