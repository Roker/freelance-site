<div class="printableSummary" style="margin:auto;width:670px">
	<center>
    <h3>Focus Audio Visual Services LTD</h3>
	<h3>Freelance Timesheet</h3>
	</center>
	<h3>Name: <?=  $user->name ?></h3>
	<h4><?= __('Pay Period: ') . $pay_period['start_date']->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) . ' - ' . $pay_period['end_date']->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]);?> </h4>
	<?=$this->cell('Shifts::displayPrintable', [$user_id, $pay_period['id']]);?>

	<div style="font-weight:bold;">
	<h4 class="summary"><?=__('Totals')?></h4>
	<h5 class="summary"><?=__('Hours: ') . $total_hours;?></h5>
	<h5 class="summary"><?=__('Earned: $') . $earned;?></h5>
	</div>
</div>

