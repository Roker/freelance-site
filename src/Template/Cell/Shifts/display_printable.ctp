<?= $this->Html->css('printable.css') ?>
<table cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th scope="col" style="width: 110px"><?=__('Date')?></th>
			<th scope="col" style="width: 150px"><?=__('Time')?> </th>
			<th scope="col" style="width: 50px"><?=__('Hours')?> </th>
			<th scope="col" style="width: 50px"><?=__('Rate')?> </th>
			<th scope="col" style="width: 75px"><?=__('Amount')?> </th>
			<th scope="col" style="width: 230px"><?=__('Notes')?> </th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($shifts as $shift): ?>
		<tr>
		
			<td><?= h($shift->date->i18nFormat('eee, M/dd/yyyy')) ?></td>
			<td><?= h($shift->start_time . ' - ' . $shift->end_time)?></td>
			<td><?= h($shift->hours) ?></td>
			<td><?= h("$" . $shift->rate) ?></td>
			<td><?= h("$" . ($shift->hours * $shift->rate)) ?></td>
			<td><?= h($shift->notes) ?></td>
			</td>
		</tr>
		<?php endforeach; ?>
		<tr style="font-weight:bold;">
			<td></td>
			<td style="text-align:right;">Totals:</td>
			<td><?= h($total_hours) ?></td>
			<td></td>
			<td><?= h("$" . $earned) ?></td>
			<td></td>
			</td>
		</tr>
	</tbody>
</table>