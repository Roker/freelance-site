<h4><?= __('Current Pay Period: ') . $start_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) . ' - ' . $end_date->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) . __(' (Displaying all unapproved shifts)');?> </h4>
<h5><?=__('Shifts: ') . $shiftCount;?></h5>
<h5><?=__('Hours: ') . $total_hours;?></h5>
<h5><?=__('Earned: $') . $earned;?></h5>

<?=$this->cell('Shifts::displayShifts', [$user_id, $shifts]);?>