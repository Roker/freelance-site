<div style="overflow-x:auto;">
<table cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th scope="col" class="shift-table shift-table-date"><?= $this->Paginator->sort('date') ?></th>
			<th scope="col" class="shift-table shift-table-time"><?=__('Time')?> </th>
			<th scope="col" class="shift-table shift-table-hours"><?=__('Hours')?> </th>
			<th scope="col" class="shift-table shift-table-rate"><?=__('Rate')?> </th>
			<th scope="col" class="shift-table shift-table-earned"><?=__('Earned')?> </th>
			<th scope="col" class="shift-table shift-table-notes"><?=__('Notes')?> </th>
			<th scope="col" class="shift-table shift-table-status"><?= $this->Paginator->sort('status') ?></th>
			<th scope="col" class="actions shift-table shift-table-actions"><?= __('Actions') ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($shifts as $shift): ?>
		<tr>
			<td><?= h($shift->date) ?></td>
			<td><?= h($shift->start_time . ' - ' . $shift->end_time)?></td>
			<td><?= h($shift->hours) ?></td>
			<td><?= h("$" . $shift->rate) ?></td>
			<td><?= h("$" . ($shift->hours * $shift->rate)) ?></td>
			<td class="shift-table-body-notes"><?= h($shift->notes) ?></td>
			<td><?= h($shift->status) ?></td>
			<td class="actions">
			<?php if(!$shift->approved){
				echo $this->Html->link(__('Edit'), ['controller' => 'shifts', 'action' => 'edit', $shift->id]);
				echo $this->Form->postLink(__('Delete'), ['controller' => 'Shifts', 'action' => 'delete', $shift->id], ['confirm' => __('Are you sure you want to delete this shift?', $shift->id)]);
			}
			?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>