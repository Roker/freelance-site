<h4><?= __('Pay Period: ') . $pay_period['start_date']->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]) . ' - ' . $pay_period['end_date']->i18nFormat([\IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE]);?> </h4>
<h5><?=__('Shifts: ') . $shift_count;?></h5>
<h5><?=__('Hours: ') . $total_hours;?></h5>
<h5><?=__('Earned: $') . $earned;?></h5>

<?=$this->cell('Shifts::display', [$user_id, $pay_period['id']]);?>