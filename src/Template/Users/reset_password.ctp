<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h('Password Reset') ?></h3>
	<p>Write the password below and send to the employee. Password will change again if you refresh this page.</p>
	<?php
	echo $user->name;
	echo '<br>';
	echo 'New Password: ' . $password;
    ?>
</div>
