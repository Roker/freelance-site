<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<?=$this->element('sidebar');?>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
	<?php
	if($hideInactive){
		echo $this->Html->link(__('View Inactive Users'), '/users/?showInactive=true');
	}else{
		echo $this->Html->link(__('Hide Inactive Users'), ['action' => 'index']);
	}
	?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">Name</th>
				<th scope="col" class="actions"><?= __('Actions') ?></th>
                <th scope="col">Email</th>
				<th scope="col">On Averaging Agreement</th>
                <th scope="col">Can Approve</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->name) ?></td>
				<td class="actions">
					<?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
					<?= $this->Html->link(__('Shifts'), ['controller' => 'pay_periods', 'action' => 'view-user', $user->id]) ?>
					<?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
					<?= $this->Html->link(__('Reset Password'), ['action' => 'reset-password', $user->id],
																['confirm' => 'Are you sure you want to reset their password?']) ?>
                </td>
                <td><?= h($user->email) ?></td>
				<td><?= h($user->on_averaging_agreement) ?></td>
				<td><?= h($user->can_approve) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
