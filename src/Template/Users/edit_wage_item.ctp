<?=$this->element('sidebar');?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($wageItem) ?>
    <fieldset>
        <legend><?= __('Edit Wage Item') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('rate');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
