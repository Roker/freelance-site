<?=$this->element('sidebar');?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($newWageItem) ?>
    <fieldset>
        <legend><?= __('Add Wage Item') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('rate');
            echo $this->Form->hidden('enabled'); 
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
