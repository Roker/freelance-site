<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<?=$this->element('sidebar');?>
<div class="users index large-9 medium-8 columns content">
    <h4><?= __('Pay Period') . ' - ' . $start_date . ' - ' . $end_date;?> </h4>
	<p><i>*Red highlights indicate a shift outside of current billing period.</i></p>
		
		<?php if(count($users) > 0): ?>
			
		<?= $this->Form->create($users); ?>
		<?php foreach ($users as $i=>$user): ?>
		<?= $this->Form->hidden($i . '.id');?>

		
		<h3><?= $this->Html->link(__($user->name), ['controller' => 'Users', 'action' => 'view', $user->id], ['target' => '_blank']) ?></h3>
		<div class="related" style="margin-bottom: 100px">
			<label><span>Approve All</span>
			<input id="checkbox" type="checkbox" onClick="checkBoxes(this, <?=$user['id']?>)" style="float: left">
			</label>
			<br>
			<?php 
			$total_hours = 0;
			foreach ($user->shifts as $shift){
				$total_hours += $shift->hours;
			} 
			
			  ?>
			<h5><?=__('Shifts: ') . count($user->shifts)?></h5>
			<h5><?=__('Hours: ') . $total_hours?></h5>
			
			<table cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th scope="col" class="shift-table shift-table-date"><?= $this->Paginator->sort('date') ?></th>
						<th scope="col" class="shift-table shift-table-time"><?=__('Time')?> </th>
						<th scope="col" class="shift-table shift-table-hours"><?=__('Hours')?> </th>
						<th scope="col" class="shift-table shift-table-rate"><?=__('Rate')?> </th>
						<th scope="col" class="shift-table shift-table-earned"><?=__('Earned')?> </th>
						<th scope="col" class="shift-table shift-table-notes"><?=__('Notes')?> </th>
						<th scope="col" class="shift-table shift-table-status"><?= $this->Paginator->sort('status') ?></th>
						<th scope="col" class="shift-table shift-table-notes"><?=__('Admin Notes')?> </th>
					</tr>
				</thead>

				<?php foreach ($user->shifts as $j=>$shift): ?>
				

				<?= $this->Form->hidden($i . '.shifts.' . $j . '.id');?>
				<?php if($shift->date < $start_date){
					echo '<tr bgcolor="red">';
				}else{
					echo '<tr>';
				}
					?>
				
					<td><?= h($shift->date) ?></td>
					<td><?= h($shift->start_time) ?></td>
					<td><?= h($shift->end_time) ?></td>
					<td><?= h($shift->hours) ?></td>
					<td><?= h($shift->labour_code) ?></td>
					<td class="shift-table-body-notes"><?= h($shift->notes) ?></td>
					<td><?= $this->Form->control($i . '.shifts.' . $j . '.approved', ['class' => $shift['user_id']]); ?></td>
					<td><?= $this->Form->text($i . '.shifts.' . $j . '.admin_notes', array('maxlength'=>'1024')); ?></td>
				</tr>
				<?php endforeach; ?>
			</table>			
		</div>
		<?php endforeach; ?>
		<?=$this->Form->submit('Save');?>
		<?php else: ?>
		<p>No unapproved shifts found.</p>
		<?php endif; ?>

</div>
<script src="https://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">	
function checkBoxes(source, user_id){
	$("." + user_id).prop("checked", source.checked);
}
</script> 
