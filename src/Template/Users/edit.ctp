<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users form large-9 medium-8 columns content">
	<?= $this->Html->link(__('Back'), ['action' => 'index']); ?>
    <?= $this->Form->create($user) ?>
	
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('on_averaging_agreement');
			echo $this->Form->control('active', ['label' => 'Active (Uncheck to hide user)']);
            echo $this->Form->control('can_approve', ['label' => 'Can Approve (Admin)']);
			//echo debug($user);
			echo '<br>';
			echo '<h3>' . 'Wage Items' . '</h3>';
			//echo $this->Form->control('user_wage_items[].enabled', ['type' => 'checkbox', 'multiple' => 'true']);
			foreach($user->user_wage_items as $i=>$wage_item){
				echo $this->Form->hidden('user_wage_items.' . $i . '.id');
				echo $this->Form->control('user_wage_items.' . $i . '.enabled', ['label' => $wage_item->name . ": $" . $wage_item->rate]);
                echo $this->Html->link(__('Edit '), ['action' => 'edit_wage_item', $wage_item->id]);
                echo $this->Html->link(__('Delete'), ['action' => 'delete_wage_item', $wage_item->id]
                                                    ,['confirm' => 'Are you sure you want to delete ' . $wage_item->name . '?']);
                                                    
                echo '<hr>';
			}
            echo '<br>';
	        echo $this->Html->link(__('Add Wage Item'), ['action' => 'add_wage_item', $user->id]);
            
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
