<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>

	<h4><?= __('All Shifts') ?></h4>

	<?=$this->cell('Shifts::display', [$user->id]);?>
</div>
