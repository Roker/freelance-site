<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h('Set Password') ?></h3>
	<?php
	echo $this->Form->create();
	echo $this->Form->control('current_password', ['type' => 'password', 'label' => 'Current Password', 'required' => true]);
	echo $this->Form->control('password', ['type' => 'password', 'label' => 'New Password', 'required' => true]);
	echo $this->Form->control('password_confirm', ['type' => 'password', 'label' => 'Confirm New Password', 'required' => true]);
	echo $this->Form->button('Submit');
    ?>
</div>
