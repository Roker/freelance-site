<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=$this->element('sidebar');?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
    </table>
	<p><?= $this->Html->link(__('Past Shifts'),  ['controller' => 'pay_periods', 'action' => 'view-user', $user->id]);?></p>
	<?=$this->cell('Shifts::displayUnapprovedWithSummary', [$user->id, $start_date, $end_date]);?>
</div>
