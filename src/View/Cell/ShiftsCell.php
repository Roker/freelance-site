<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Datasource\Paginator;


class ShiftsCell extends Cell
{
    public function display($user_id, $start_date = null, $end_date = null)
    {
		$shifts = $this->GetShifts($user_id, $start_date, $end_date);
		
		$this->set('shifts', $shifts);
    }
	
	public function displayShifts($user_id, $shifts)
    {	
		$this->set('shifts', $shifts);
    }
	
	public function displayPrintable($user_id, $pay_period_id)
    {
		$shifts = $this->GetShifts($user_id, $pay_period_id);

		
		$total_hours = 0;
		$earned = 0;
		foreach($shifts as $shift){
			$total_hours += $shift->hours;
			$earned += $shift->hours * $shift->rate;
		}
		
		$this->set('shifts', $shifts);
		$this->set('total_hours', $total_hours);
		$this->set('earned', $earned);
		$this->set('pay_period_id', $pay_period_id);
		$this->set('user_id', $user_id);
		
		
		$this->set(compact('shifts', 'hours', 'earned'));
	
    }
	
	
	public function displayUnapprovedWithSummary($user_id, $start_date = null, $end_date = null){
		$this->loadModel('Shifts');
		
		$shifts = $this->Shifts->find('all')->matching('Users', function ($q) use ($user_id){ return $q->where(['user_id' => $user_id]); })->where(['approved' => false])->order(['date' => 'ASC']);

		$total_hours = 0;
		$earned = 0;
		$shiftCount = 0;
		foreach($shifts as $shift){
			$total_hours += $shift->hours;
			$earned += $shift->hours * $shift->rate;
			$shiftCount++;
		}
		$this->set('shiftCount', $shiftCount);
		$this->set('shifts', $shifts);
		$this->set('total_hours', $total_hours);
		$this->set('earned', $earned);
		$this->set('start_date', $start_date);
		$this->set('end_date', $end_date);
		$this->set('user_id', $user_id);
	}
	
	public function displayWithSummary($user_id, $pay_period_id){
		$shifts = $this->GetShifts($user_id, $pay_period_id);

		
		$total_hours = 0;
		$earned = 0;
		foreach($shifts as $shift){
			$total_hours += $shift->hours;
			$earned += $shift->hours * $shift->rate;
		}
		
		$this->set('shifts', $shifts);
		$this->set('total_hours', $total_hours);
		$this->set('earned', $earned);
		$this->set('pay_period_id', $pay_period_id);
		$this->set('user', $user_id);
		
		
	}
	
	public function displayWithSummaryPrintable($user_id, $pay_period_id){
		
		$this->loadModel('Users');
		$user = $this->Users->get($user_id);
		
		//echo(debug($user);
		
		$this->set('user', $user);
		$this->set('pay_period_id', $pay_period_id);
	}
	public function displayPayPeriod($user_id, $pay_period_id){
		$this->loadModel('Shifts');
		$this->loadModel('PayPeriods');
		$pay_period = $this->PayPeriods->get($pay_period_id);
		$shifts = $this->Shifts->find('all')
			->where(['user_id' => $user_id])->andWhere(['pay_period_id' => $pay_period_id])->order(['date' => 'ASC']);
		
		
		$this->set('shifts', $shifts);
		$this->set('start_date', $pay_period['start_date']);
		$this->set('end_date', $pay_period['end_date']);
		$this->set('user_id', $user_id);

		$this->viewBuilder()->setTemplate('display');
		
	}
	public function displayPayPeriodWithSummary($user_id, $pay_period_id, $printable=false){
		$this->loadModel('Shifts');
		$this->loadModel('PayPeriods');
		$pay_period = $this->PayPeriods->get($pay_period_id);
		$shifts = $this->Shifts->find('all')
			->where(['user_id' => $user_id])->andWhere(['pay_period_id' => $pay_period_id])->order(['date' => 'ASC']);

		$total_hours = 0;
		$earned = 0;
		$shift_count = 0;
		foreach($shifts as $shift){
			$total_hours += $shift->hours;
			$earned += $shift->hours * $shift->rate;
			$shift_count += 1;
		}
		
		
		$this->set('shifts', $shifts);
		$this->set('total_hours', $total_hours);
		$this->set('shift_count', $shift_count);
		$this->set('earned', $earned);
		$this->set('pay_period', $pay_period);
		$this->set('user_id', $user_id);
		if($printable){
			$this->loadModel('Users');
			$this->set('user', $this->Users->get($user_id));

			$this->viewBuilder()->setTemplate('displayWithSummaryPrintable');
		}else{
			$this->viewBuilder()->setTemplate('displayWithSummary');
		}
		
		
	}

	private function GetShifts($user_id, $pay_period_id){
		$this->loadModel('Shifts');
		$shifts = $this->Shifts->find('all')
			->where(['user_id' => $user_id])->andWhere(['pay_period_id' => $pay_period_id])->order(['date' => 'ASC']);
			
			
		// $paginator = new Paginator();
		// $shifts = $paginator->paginate($query);
		// $this->request = $this->request->withParam(
			// 'paging',
			// $paginator->getPagingParams() + (array)$this->request->getParam('paging')
		// );
		
		return $shifts;
	}
	
	// private function GetShifts($user_id, $start_date, $end_date){
		// $this->loadModel('Shifts');
		// $query = $this->Shifts->find('all')
			// ->where(['user_id' => $user_id])->order(['date' => 'ASC']);
			
		// if($start_date){
			// $query = $query->andWhere(['date >=' => $start_date]);
		// }
		
		// if($end_date){
			// $query = $query->andWhere(['date <=' => $end_date]);
		// }

			
		// $paginator = new Paginator();
		// $shifts = $paginator->paginate($query);
		// $this->request = $this->request->withParam(
			// 'paging',
			// $paginator->getPagingParams() + (array)$this->request->getParam('paging')
		// );
		
		// return $shifts;
	// }

}