<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WageItem Entity
 *
 * @property int $id
 * @property string $qb_id
 * @property string $name
 *
 * @property \App\Model\Entity\Qb $qb
 * @property \App\Model\Entity\UserWageItem[] $user_wage_items
 */
class WageItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'qb_id' => true,
        'name' => true,
        'qb' => true,
        'user_wage_items' => true
    ];
}
