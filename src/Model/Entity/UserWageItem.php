<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserWageItem Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property float $rate
 * @property bool $enable
 *
 * @property \App\Model\Entity\User $user
 */
class UserWageItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'rate' => true,
        'enabled' => true,
        'user' => true,
		'qb_id' => true
    ];
}
