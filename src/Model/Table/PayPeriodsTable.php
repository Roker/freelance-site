<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;	
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;

/**
 * PayPeriods Model
 *
 * @property \App\Model\Table\ShiftsTable|\Cake\ORM\Association\HasMany $Shifts
 *
 * @method \App\Model\Entity\PayPeriod get($primaryKey, $options = [])
 * @method \App\Model\Entity\PayPeriod newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PayPeriod[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PayPeriod|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayPeriod|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayPeriod patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PayPeriod[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PayPeriod findOrCreate($search, callable $callback = null, $options = [])
 */
class PayPeriodsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pay_periods');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Shifts', [
            'foreignKey' => 'pay_period_id'
        ]);
		
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->date('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmpty('end_date');

        return $validator;
    }
	
	public function GetCurrentPayPeriod(){
		$this->GetBillingPeriod($start_date, $end_date);
		$today = Time::now();
		
		$pay_period = $this->find('all')->first();
		
		if(!isset($pay_period) || $today > $pay_period['end_date']){
			$pay_period = $this->newEntity();
			$pay_period['start_date'] = $start_date;
			$pay_period['end_date'] = $end_date;
			$this->save($pay_period);
		}
		
		
		return $pay_period;
	}
	
	public function GetPayPeriodId($date){
		$pay_period = $this->find('all')->where(['start_date <=' => $date])->andWhere(['end_date >=' => $date])->first();
		if($pay_period == null){
			$pay_period = $this->newEntity();
			
			$this->GetBillingPeriod($start_date, $end_date);
				
			
			while($start_date > $date){
				$start_date = $start_date->addDays(-14);
			}
			
						
			$end_date = clone $start_date;
			$end_date->addDays(13);
			
			
			$pay_period['start_date'] = $start_date;
			$pay_period['end_date'] = $end_date;
			
			$this->save($pay_period);
		}
		return $pay_period['id'];
	}
	
	public function GetBillingPeriod(&$start_date, &$end_date, $offset=null){
		$now = Date::now();
		$start_date = new Date('2018-07-8');
		
		$compDate = $now->addDays(-14);
		if($offset){
			$compDate->addDays($offset);
		}
		
		while($start_date <= $compDate){
			$start_date->addDays(14);
		}

		$end_date = clone $start_date;
		$end_date->addDays(13);
	}
}
