<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;
use Cake\ORM\TableRegistry;



/**
 * Shifts Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Shift get($primaryKey, $options = [])
 * @method \App\Model\Entity\Shift newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Shift[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Shift|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shift|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shift patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Shift[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Shift findOrCreate($search, callable $callback = null, $options = [])
 */
class ShiftsTable extends Table
{

    /**
     * Initialize method
     *xml_error_string
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('shifts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
		
		$this->belongsTo('UserWageItems', [
            'foreignKey' => 'user_wage_item_id',
            'joinType' => 'INNER'
        ]);
		
		$this->belongsTo('PayPeriods', [
            'foreignKey' => 'pay_period_id',
            'joinType' => 'INNER'
        ]);
		

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');
			
		$validator
            ->date('pay_period_id')
            ->requirePresence('pay_period_id', 'create')
            ->notEmpty('pay_period_id');

        $validator
            ->time('start_time')
            ->requirePresence('start_time', 'create')
            ->notEmpty('start_time');

        $validator
            ->time('end_time')
            ->requirePresence('end_time', 'create')
            ->notEmpty('end_time');
			
        $validator
            ->scalar('notes')
			->allowEmpty('notes', ['create', 'edit'])
            ->maxLength('notes', 1024);
			
		$validator
            ->scalar('admin_notes')
			->allowEmpty('admin_notes', ['create', 'edit'])
            ->maxLength('admin_notes', 1024);
			
		$validator->add('end_time', 'comparison', [
			'rule' => function ($value, $context) {
				return $value != $context['data']['start_time'];
			},
			'message' => 'Start time and end time must be different.'
		]);
			
		$validator->add('date', 'custom', [
			'rule' => function ($value, $context) {
				$date = new Date($value['year'] . "-" . $value['month'] . "-" . $value['day']);
				return $date <= Date::now();
			},
			'message' => 'Shift date must be today or earlier.'
		]);


        return $validator;
    }
	
	public function beforeSave($event, $entity, $options) {
		$user_wage_items = TableRegistry::getTableLocator()->get('UserWageItems');

		$wage_item = $user_wage_items->get($entity['user_wage_item_id']);
		
		$entity['rate'] = $wage_item['rate'];
		
		if($entity['start_time'] <= $entity['end_time']){
			$entity['hours'] = round($entity['start_time']->diffInMinutes($entity['end_time'])/60, 2);
		}else{
			$entity['hours'] = 24 - round($entity['start_time']->diffInMinutes($entity['end_time'])/60, 2);
		}
				
		if($entity['closed']){
			$entity['status'] = "Paid";
		}else if($entity['approved']){
			$entity['status'] = "Approved";
		}
	}

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
		$rules->add($rules->existsIn(['pay_period_id'], 'PayPeriods'));

        return $rules;
    }
}
