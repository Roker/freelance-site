<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WageItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WageItemsTable Test Case
 */
class WageItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WageItemsTable
     */
    public $WageItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wage_items',
        'app.qbs',
        'app.user_wage_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WageItems') ? [] : ['className' => WageItemsTable::class];
        $this->WageItems = TableRegistry::getTableLocator()->get('WageItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WageItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
